#!/bin/bash
set -e
set -x


if [[ "$1" =~ "app" ]] # Install dependencies for runtime
then
  echo "Installing runtime dependencies ..."

else # Install dependencies for development
  echo "Installing development dependencies ..."

fi